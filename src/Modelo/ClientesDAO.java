package Modelo;

import java.util.ArrayList;

public interface ClientesDAO {
    ArrayList<Clientes> select_all();
    Clientes select_by_id(int id);
    boolean insert_cliente(Clientes cliente);
    boolean delete_cliente_by_id(int id);
}

