package Modelo;

import java.util.ArrayList;

public interface ProductosDAO {
    ArrayList<Productos> select_all();
    Productos select_by_id(int id);
    boolean insert_producto(Productos producto);
    boolean delete_producto_by_id(int id);
    boolean update_stock_by_id(int id, int stock);
}
