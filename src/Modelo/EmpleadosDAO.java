package Modelo;

import java.util.ArrayList;

public interface EmpleadosDAO {
    ArrayList<Empleados> select_all();
    Empleados select_by_id(int id);
    double select_avg_salary();
    boolean insert_user(Empleados user);
    boolean delete_user_by_id(int id);
    boolean update_salary_by_id(int id, double salary);
}
