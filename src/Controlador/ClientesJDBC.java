package Controlador;

import Modelo.Clientes;
import Modelo.ClientesDAO;

import java.sql.*;
import java.util.ArrayList;

public class ClientesJDBC implements ClientesDAO {

    public final String SELECT_ALL = "SELECT * FROM Clientes";
    public final String SELECT_BY_ID = "SELECT * FROM Clientes WHERE id = ?";
    public final String INSERT_CLIENTE = "INSERT INTO Clientes (nombre, apellido, email, telefono, fecha_registro) VALUES (?, ?, ?, ?, ?)";
    public final String DELETE_CLIENTE_BY_ID = "DELETE FROM Clientes WHERE id = ?";

    Connection con;

    public ClientesJDBC(Connection con){
        this.con = con;
    }

    @Override
    public ArrayList<Clientes> select_all() {
        ArrayList<Clientes> lista_clientes = new ArrayList<>();

        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_ALL);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("id");
                String nombre = rs.getString("nombre");
                String apellido = rs.getString("apellido");
                String email = rs.getString("email");
                String telefono = rs.getString("telefono");
                Date fecha_registro = rs.getDate("fecha_registro");

                Clientes c = new Clientes(id, nombre, apellido, email, telefono, fecha_registro);
                lista_clientes.add(c);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return lista_clientes;
    }

    public Clientes select_by_id(int id) {
        Clientes cliente = null;

        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_BY_ID);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                String nombre = rs.getString("nombre");
                String apellido = rs.getString("apellido");
                String email = rs.getString("email");
                String telefono = rs.getString("telefono");
                Date fecha_registro = rs.getDate("fecha_registro");

                cliente = new Clientes(id, nombre, apellido, email, telefono, fecha_registro);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return cliente;
    }


    @Override
    public boolean insert_cliente(Clientes cliente) {
        try {
            PreparedStatement stmt = con.prepareStatement(INSERT_CLIENTE);
            stmt.setString(1, cliente.getNombre());
            stmt.setString(2, cliente.getApellido());
            stmt.setString(3, cliente.getEmail());
            stmt.setString(4, cliente.getTelefono());
            stmt.setDate(5, new java.sql.Date(cliente.getFecha_registro().getTime()));
            int rows = stmt.executeUpdate();
            return rows > 0;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean delete_cliente_by_id(int id) {
        try {
            PreparedStatement stmt = con.prepareStatement(DELETE_CLIENTE_BY_ID);
            stmt.setInt(1, id);
            int rows = stmt.executeUpdate();
            return rows > 0;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}


