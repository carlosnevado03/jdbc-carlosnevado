package Controlador;

import Modelo.Productos;
import Modelo.ProductosDAO;

import java.sql.*;
import java.util.ArrayList;

public class ProductosJDBC implements ProductosDAO {

    public final String SELECT_ALL = "SELECT * FROM Productos";
    public final String SELECT_BY_ID = "SELECT * FROM Productos WHERE id = ?";
    public final String INSERT_PRODUCTO = "INSERT INTO Productos (nombre, precio, stock) VALUES (?, ?, ?)";
    public final String DELETE_PRODUCTO_BY_ID = "DELETE FROM Productos WHERE id = ?";
    public final String UPDATE_STOCK_BY_ID = "UPDATE Productos SET stock = ? WHERE id = ?";

    Connection con;

    public ProductosJDBC(Connection con){
        this.con = con;
    }

    @Override
    public ArrayList<Productos> select_all() {
        ArrayList<Productos> lista_productos = new ArrayList<>();

        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_ALL);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("id");
                String nombre = rs.getString("nombre");
                double precio = rs.getDouble("precio");
                int stock = rs.getInt("stock");

                Productos p = new Productos(id, nombre, precio, stock);
                lista_productos.add(p);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return lista_productos;
    }

    public Productos select_by_id(int id) {
        Productos producto = null;

        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_BY_ID);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                String nombre = rs.getString("nombre");
                double precio = rs.getDouble("precio");
                int stock = rs.getInt("stock");

                producto = new Productos(id, nombre, precio, stock);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return producto;
    }

    @Override
    public boolean insert_producto(Productos producto) {
        try {
            PreparedStatement stmt = con.prepareStatement(INSERT_PRODUCTO);
            stmt.setString(1, producto.getNombre());
            stmt.setDouble(2, producto.getPrecio());
            stmt.setInt(3, producto.getStock());
            int rows = stmt.executeUpdate();
            return rows > 0;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean delete_producto_by_id(int id) {
        try {
            PreparedStatement stmt = con.prepareStatement(DELETE_PRODUCTO_BY_ID);
            stmt.setInt(1, id);
            int rows = stmt.executeUpdate();
            return rows > 0;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean update_stock_by_id(int id, int stock) {
        try {
            PreparedStatement stmt = con.prepareStatement(UPDATE_STOCK_BY_ID);
            stmt.setInt(1, stock);
            stmt.setInt(2, id);
            int rows = stmt.executeUpdate();
            return rows > 0;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}

