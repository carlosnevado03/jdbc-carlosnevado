package Vista;

import Controlador.ProductosJDBC;
import Modelo.Productos;
import Utilidades.Conexion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Conexion c = new Conexion();
        Connection conn = null;
        PreparedStatement stmt = null;
        Scanner scanner = new Scanner(System.in);

        try {
            System.out.println("Conectando a la base de datos...");
            conn = c.getConnection();
            ProductosJDBC controller = new ProductosJDBC(conn);

            int opcion;
            do {
                System.out.println("\nMenú:");
                System.out.println("1. Mostrar todos los productos");
                System.out.println("2. Mostrar producto por ID");
                System.out.println("3. Agregar nuevo producto");
                System.out.println("4. Eliminar producto por ID");
                System.out.println("5. Actualizar stock de producto por ID");
                System.out.println("0. Salir");
                System.out.print("Seleccione una opción: ");
                opcion = scanner.nextInt();

                switch (opcion) {
                    case 1:
                        ArrayList<Productos> listaProductos = controller.select_all();
                        for (Productos producto : listaProductos) {
                            System.out.println(producto);
                        }
                        break;
                    case 2:
                        System.out.print("Introduce el ID del producto: ");
                        int idProducto = scanner.nextInt();
                        Productos producto = controller.select_by_id(idProducto);
                        if (producto != null) {
                            System.out.println(producto);
                        } else {
                            System.out.println("No se encontró ningún producto con ese ID.");
                        }
                        break;
                    case 3:
                        System.out.println("Insertar nuevo producto:");
                        // Solicitar datos al usuario y crear un nuevo objeto Productos
                        // Llamar al método insert_producto de controller con el nuevo objeto Productos
                        break;
                    case 4:
                        System.out.print("Introduce el ID del producto a eliminar: ");
                        int idEliminar = scanner.nextInt();
                        boolean eliminado = controller.delete_producto_by_id(idEliminar);
                        if (eliminado) {
                            System.out.println("Producto eliminado correctamente.");
                        } else {
                            System.out.println("No se pudo eliminar el producto.");
                        }
                        break;
                    case 5:
                        System.out.print("Introduce el ID del producto: ");
                        int idActualizar = scanner.nextInt();
                        System.out.print("Introduce el nuevo stock: ");
                        int nuevoStock = scanner.nextInt();
                        boolean actualizado = controller.update_stock_by_id(idActualizar, nuevoStock);
                        if (actualizado) {
                            System.out.println("Stock actualizado correctamente.");
                        } else {
                            System.out.println("No se pudo actualizar el stock.");
                        }
                        break;
                    case 0:
                        System.out.println("Saliendo del programa...");
                        break;
                    default:
                        System.out.println("Opción no válida. Por favor, seleccione una opción del menú.");
                        break;
                }
            } while (opcion != 0);
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (Exception ignored) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception ignored) {
            }
        }
    }
}
